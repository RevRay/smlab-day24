package ru.smlab.school.day24.logistics;

public class Truck {
    String model;
    double tonnage;
    boolean isFunctioning;

    public Truck(String model, double tonnage, boolean isFunctioning) {
        this.model = model;
        this.tonnage = tonnage;
        this.isFunctioning = isFunctioning;
    }

    @Override
    public String toString() {
        return "Truck{" +
                "model='" + model + '\'' +
                ", tonnage=" + tonnage +
                ", isFunctioning=" + isFunctioning +
                '}';
    }
}
