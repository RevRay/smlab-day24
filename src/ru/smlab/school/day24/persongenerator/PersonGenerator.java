package ru.smlab.school.day24.persongenerator;

import java.util.*;
import java.util.function.BinaryOperator;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Задание 1
 *
 * На базе массива имен - сгенрировать объекты типа человек (Person)
 * со случайным возрастом, после - вывести каждый объект в консоль.
 * Отсортировать объекты по возрасту по возрастанию,
 * снова вывести каждого в консоль.
 *
 *
 * Задание 2
 *
 * Сделать то же самое, но на базе массива имен сгенерировать 30 объектов человек.
 */
public class PersonGenerator {
    public static void main(String[] args) {
        String[] availableNames = {
                "Федор",
                "Петр",
                "Ирина",
                "Ксения",
                "Влад",
                "Лана",
                "Марк"
        };

        Random r = new Random();

        //(string) -> new Person
//        Arrays.stream(availableNames)
//                .map(Person::new)
//                .peek(System.out::print)
//                .sorted((o1, o2) -> Integer.compare(o1.age, o2.age))
//                .forEach(System.out::println);


        Stream.of("ab", "cd", "ef").reduce(String::concat);


        List<String> names = Arrays.asList(availableNames);

//        Stream.generate(() -> availableNames[r.nextInt(availableNames.length)])
//                .limit(30)
//                .map(s -> new Person(s))
//                .peek(System.out::print)
//                .sorted((o1, o2) -> Integer.compare(o1.age, o2.age))
//                .forEach(System.out::println);

//        Stream<String> names1 = List.of("Ваня", "Петя").stream();
//        Stream<String> names2 = List.of("Макс", "Робин").stream();
//
//        Stream.concat(names1, names2)
//                .forEach(System.out::println);



        List<Person> people = List.of(
                new Person("Василий", 12),
                new Person("Людвиг", 23),
                new Person("Елена", 34),
                new Person("Марина", 43),
                new Person("Татьяна", 25),
                new Person("Станислав", 17)
        );

        Person found = people.stream()
                .filter((p1) -> p1.age >= 180)
                .findAny() //
                .orElseThrow(() -> new RuntimeException("exception from optional"));

//        if (found.isEmpty()){
//            throw new RuntimeException("$#$#@%#%#%");
//        }
        System.out.println(found.age);

//        Map<String, Integer> map = new HashMap<>();

    }
}
