package ru.smlab.school.day24.persongenerator;

import java.util.Random;

public class Person{
    String name;
    int age;

    static Random r = new Random();

    public Person(String name) {
        this.name = name;
        this.age = r.nextInt(150);
    }

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "{" +
                "имя='" + name + '\'' +
                ",возраст=" + age +
                '}';
    }

}
