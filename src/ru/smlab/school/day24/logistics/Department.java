package ru.smlab.school.day24.logistics;

public enum Department {
    EAST,
    WEST,
    NORTH,
    SOUTH
}
