#### Functional interfaces
Consumer<T> Supplier<T>  
Predicate<T> Function<T,R>  
UnaryOperator<T,T> BiConsumer<T,U>  
BiFunction<T,U,R>  
BinaryOperator<T,T,T>
#### Terminal operations:  
toArray  collect    
count  reduce 
forEach  forEachOrdered  
min  max 
anyMatch  allMatch  
noneMatch  findAny  
findFirst 
#### Conveyor operations  
filter  skip  
distinct  map  
peek  limit  
sorted  mapToInt  
mapToDouble  mapToLong  
flatMap  flatMapToInt  
flatMapToDouble  flatMapToLong