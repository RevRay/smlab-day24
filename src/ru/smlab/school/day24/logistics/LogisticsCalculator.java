package ru.smlab.school.day24.logistics;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.ToDoubleFunction;
import java.util.stream.Stream;

/**
 * Задание 3.
 * Мы - логистическая компания и работаем с сервисом, хранящим данные
 * о всей используемой в компании грузовой технике, по всем департаментам компании
 * (южный, северный, восточный, западный)
 *
 * В компанию пришел особо крупный заказ на перевозку 2000 тонн конфет рафаэлло, и
 * нам необходимо:
 * 1. вывести на экран все грузовики в собственности компании
 * 2. посчитать грузоподъемность всех рабочих грузовиков компании
 *
 * Задание 4.
 * Найти любой грузовик, который способен перевозить более 30 тонн.
 * Если такого грузовика нет - выбросите исключение TruckNotFoundException
 *
 */
public class LogisticsCalculator {
    public static void main(String[] args) {
        //simulate API call
        Map<Department, List<Truck>> allCompanyTrucks = fetchTransportData();


        double result = allCompanyTrucks.entrySet().stream()
                .map(e -> e.getValue())
                .flatMap(trucks -> trucks.stream())
                .filter(t -> t.isFunctioning)
                .peek(System.out::println)
                .mapToDouble(t -> t.tonnage)
                .sum();

        System.out.println(result);

    }


//        System.out.println(allCompanyTrucks.entrySet().stream()
//                .peek(departmentListEntry -> System.out.println(departmentListEntry.getValue()))
//            .mapToDouble(new ToDoubleFunction<Map.Entry<Department, List<Truck>>>() {
//        @Override
//        public double applyAsDouble(Map.Entry<Department, List<Truck>> departmentListEntry) {
//            return departmentListEntry.getValue().stream()
//                    .filter(t -> t.isFunctioning)
//                    .mapToDouble(new ToDoubleFunction<Truck>() {
//                        @Override
//                        public double applyAsDouble(Truck truck) {
//                            return truck.tonnage;
//                        }
//                    }).sum();
//        }
//    }).sum());




    private static Map<Department, List<Truck>> fetchTransportData(){
        Map<Department, List<Truck>> trucks = new HashMap<>();
        trucks.put(
                Department.EAST,
                List.of(
                        new Truck("Scania", 20, true),
                        new Truck("Scania", 25, false),
                        new Truck("DAF", 22, true),
                        new Truck("Mercedes", 25, true)
                )
        );
        trucks.put(
                Department.WEST,
                List.of(
                        new Truck("МАЗ", 22, false),
                        new Truck("Suzuki", 25, false),
                        new Truck("Suzuki", 18, true),
                        new Truck("Volvo", 18, true),
                        new Truck("МАЗ", 15, true)
                )
        );
        trucks.put(
                Department.NORTH,
                List.of(
                        new Truck("Volvo", 20, true),
                        new Truck("Scania", 25, true),
                        new Truck("DAF", 22, true)
                )
        );
        trucks.put(
                Department.SOUTH,
                List.of(
                        new Truck("MAN", 15, true),
                        new Truck("MAN", 25, false),
                        new Truck("Mercedes", 25, true),
                        new Truck("Volvo", 21, true)
                )
        );

        return trucks;
    }
}
