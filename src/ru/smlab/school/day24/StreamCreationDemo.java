package ru.smlab.school.day24;

import java.util.function.Supplier;
import java.util.stream.Stream;

public class StreamCreationDemo {
    public static void main(String[] args) {
        Stream.of(11, 12);
        Stream.generate(() -> "dsf" + System.currentTimeMillis()).limit(30).forEach(System.out::println);
    }
}
